/**
 * Created by karankohli13 on 05/11/17.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs'),
    presets = require('../preset'),
    dates = require('../utils/datesHelper'),
    uuid = require('uuid'),
    _ = require('lodash');


var model = new Schema({

    _id: { type: String, default: uuid.v4 },
    userId: {
        type: String,
        required: true,
        unique: true
    },
    createdAt: { type: Number, default: null },
    verifiedOn: { type: Number, default: null },
    attempts: { type: Number, default: 1, select: false },
    kycStatus: { type: String, default: "pending" },
    kycVerifiedBy: { type: String, default: null },
    kycComment: { type: String, default: null },
    kyc: {
        identity: {
            number: { type: String, default: null },
            picture: { type: String, default: null },
        },
        image: {
            number: { type: String, default: null },
            picture: { type: String, default: null },
        },
        cheque: {
            picture: { type: String, default: null },
            number: { type: String, default: null }
        },
        passport: {
            picture: { type: String, default: null },
            number: { type: String, default: null }
        },
    },
    bankDetail: {
        accNumber: { type: String, default: null },
        ifsc: { type: String, default: null },
        bankName: { type: String, default: null },
    },
    address: {
        flat: { type: String, default: null },
        street: { type: String, default: null },
        state: { type: String, default: null },
        pincode: { type: Number, default: null },
        country: { type: String, default: null }
    },
});

model.pre(
    'save',
    function(next) {
        var model = this;
        model.createdAt = dates.unixTimestamp();
        next();
    });
model.pre(
    'update',
    function(next) {
        var model = this;
        model.updatedAt = dates.unixTimestamp();
        next();
    });

module.exports = mongoose.model('kyc', model);