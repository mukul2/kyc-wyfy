/**
 * Created by karankohli13 on 05/11/17.
 */
var HttpStatus = require('http-status-codes');

var handler = {

    /*GENERAL*/

    NOT_AUTHORISED: "Not Authorised",
    INVALID_ACCESS: "No Access",
    WRONG_PSWD: "Wrong password",
    WRONG_CNF_PSWD: "Confirm password didn't match",
    WRONG_OLD_PSWD: "Existing Password didn't match",
    WRONG_PARAMS: "Invalid Username or Password",
    ARG_MISSING: "Arguments Missing",
    ARG_WRONG: "Arguments Wrong",

    /*USER*/
    ADMIN_NOT_FOUND: "Admin Not Found",
    NO_RECORDS: "0 Record(s)",
    PSWD_SET:"Password Changed",

    //SUPERADMIN
    NOT_SUPER_ADMIN: "You are not a valid SUPERADMIN",
    PARAMS_MISSING: "Params are missing",
    ALREADY_EXISTS:"Already Exists",

    // UPLOAD
    MAX_FILE_SIZE: "File size is larger than 2 Mbs",
    FILE_NOT_ADDED: "File not added",
    INVALID_ACTION: "This action is not allowed",
    INVALID_PARAMS: "Invalid params",
    DOC_UPLOADED: 'Your Document uploaded successfully',


    sendErr: function(err, res, data) {
        res.status(err).send({ status: false, statusMessage: HttpStatus.getStatusText(err), data: data });
    },

    sendRes: function(data, res, msg) {
        res.status(HttpStatus.OK).send({ status: true, message: msg || null, data: data });
    }
};

module.exports = handler;