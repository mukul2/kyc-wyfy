/**
 * Created by karankohli13 on 06/11/17.
 */
var jsonwebtoken = require('jsonwebtoken'),
    adminSchema = require('../models/admin'),
    adminFunc = require('./../model_functions/admin'),
    config = require('./../../config'),
    HttpStatus = require('http-status-codes'),
    handler = require('./handler'),
    moment = require('moment'),
    env = process.env.NODE_ENV || 'development',
    date = require('./datesHelper'),
    preset = require('./../preset'),
    crypto = require('crypto-js/aes'),
    CryptoJS = require("crypto-js"),
    Recaptcha = require('express-recaptcha'),
    request = require('request');
// recaptcha = new Recaptcha(config.recaptcha.site_key, config.recaptcha.secret_key);


moment.locale('in');

var regex = {
    /* ToDo Identify phone numbers more precisely */
    phoneRegex: /^\+(?:[0-9]●?){6,14}[0-9]$/,
    emailRegex: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
};
var patt = '^[A-Z]{2}[0-9]{1,2}(?:[A-Z])?(?:[A-Z]*)?[0-9]{4}$';

var cryptoSettings = {
    password: process.env.secret || 'superSecretUser'
};

var decide = function(model, decoded, req, res, next) {
    if (model) {
        if (model.isActive) {
            req.details = model;
            next();
        } else {
            handler.sendErr(handler.ACCOUNT_SUSPENDED, res);
        }
    } else {
        handler.sendErr(handler.TOKEN_EXPIRED, res);
    }
};

var obj = {
    sendErerifyr: true,
    userTokenKey: process.env.jwtSecret || 'superSecretUser',
    accessTokenKey: process.env.accessSecret || 'superSecretUser',
    isPhone: function(phoneNumber) {
        return (regex.phoneRegex).test(phoneNumber);
    },
    isEmail: function(email) {
        return (regex.emailRegex).test(email);
    },
    isNumber: function(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    },
    identifyUsername: function(username) {
        if (this.isEmail(username)) {
            return 'email';
        } else if (this.isNumber(username)) {
            return 'phone';
        } else {
            return 0;
        }
    },
    getDate: function() {
        return moment().format('DD-MM-YYYY');
    },
    getDay: function() {
        return moment().format('ddd');
    },
    encrypt: function(text) {
        return crypto.encrypt(text, cryptoSettings.password);
    },
    decrypt: function(cipher) {
        return crypto.decrypt(cipher.toString(), cryptoSettings.password);
    },
    createCustomToken: function(data) {
        return jsonwebtoken.sign(data, obj.userTokenKey, { expiresIn: '20min' }); //expire in 2 minutes
    },
    decodeCustomToken: function(token, cb) {
        return jsonwebtoken.verify(token, obj.userTokenKey, function(err, decoded) {
            if (err) {
                cb(err);
            } else {
                cb(null, decoded);
            }
        });
    },
    createToken: function(model) {
        return jsonwebtoken.sign({
            _id: model._id,
            state: model.state,
            role: model.role,
            expire: date.addToTimestamp(preset.user.tokenExpiryTime * 6)
        }, obj.userTokenKey);
    },
    createAccessToken: function(model) {
        return jsonwebtoken.sign({
            _id: model._id,
            state: model.state,
            expire: date.addToTimestamp(preset.user.tokenExpiryTime * 6)
        }, obj.accessTokenKey);
    },
    verifyAccessToken: function(req, res, next) {
        var token = req.headers['x-access-token'];
        var key = obj.accessTokenKey;
        jsonwebtoken.verify(token, key, function(err, decoded) {
            if (err) {
                handler.sendErr(handler.NOT_AUTHORISED, res);
            } else {
                if (decoded._id) {
                    userSchema.findOne({ _id: decoded._id }, function(err, model) {
                        if (model) {
                            if (model._id === decoded._id) {
                                if (model.isActive) {
                                    req.details = model;
                                    req.details.access = true;
                                    next();
                                } else {
                                    handler.sendErr(handler.ACCOUNT_SUSPENDED, res);
                                }
                            } else {
                                handler.sendErr(err.message, res);
                            }
                        } else handler.sendErr(handler.USER_NOT_FOUND, res);
                    });
                } else {
                    req.body = decoded;
                    return next()
                }
            }
        });
    },
    verifyUser: function(req, res, next) {
        var token = req.headers['x-access-token'];
        var options = {
            url: config.userauth,
            headers: {
                'x-access-token': token
            }
        };
        request(options, (err, HTTPresponse, body) => {
            body = JSON.parse(body);
            if (err) handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
            if (body) {
                if (body.success) {
                    req.user = body.data;
                    next();
                } else handler.sendErr(HttpStatus.FORBIDDEN, res, body.message);
            }
        });

    },
    verifyAllAdmin: function(req, res, next) {
        var token = req.headers['x-access-token'] || req.params.token;
        var key = obj.userTokenKey;
        jsonwebtoken.verify(token, key, function(err, decoded) {
            if (err) {
                handler.sendErr(HttpStatus.UNAUTHORIZED, res, handler.NOT_AUTHORISED);
            } else {
                if (decoded._id) {
                    adminSchema.findOne({ _id: decoded._id }, function(err, model) {
                        if (model) {
                            if (model._id === decoded._id) {
                                req.admin = model;
                                req.admin.access = true;
                                next();
                            } else {
                                handler.sendErr(HttpStatus.NOT_FOUND, res, handler.USER_NOT_FOUND);
                            }
                        } else handler.sendErr(HttpStatus.NOT_FOUND, res, handler.USER_NOT_FOUND);
                    });
                } else {
                    req.body = decoded;
                    return next()
                }

            }
        });
    },
    verifyAdmin: function(req, res, next) {
        var token = req.headers['x-access-token'] || req.params.token;
        var key = obj.userTokenKey;
        jsonwebtoken.verify(token, key, function(err, decoded) {
            if (err) {
                handler.sendErr(HttpStatus.UNAUTHORIZED, res, handler.NOT_AUTHORISED);
            } else {
                if (decoded._id && decoded.role == "SUPERVISOR") {
                    adminSchema.findOne({ _id: decoded._id }, function(err, model) {
                        if (model) {
                            if (model._id === decoded._id) {
                                req.admin = model;
                                req.admin.access = true;
                                next();
                            } else {
                                handler.sendErr(HttpStatus.NOT_FOUND, res, handler.USER_NOT_FOUND);
                            }
                        } else handler.sendErr(HttpStatus.NOT_FOUND, res, handler.USER_NOT_FOUND);
                    });
                } else {
                    return handler.sendErr(HttpStatus.FORBIDDEN, res, 'Invalid admin');
                }

            }
        });
    },
    verifySuperAdmin: function(req, res, next) {
        var token = req.headers['x-access-token'] || req.params.token;
        var key = obj.userTokenKey;
        var _obj = {};
        jsonwebtoken.verify(token, key, function(err, decoded) {
            if (err) {
                return handler.sendErr(HttpStatus.UNAUTHORIZED, res, handler.NOT_AUTHORISED);
            } else {
                if (decoded._id && decoded.role == "SUPERADMIN") {
                    _obj.role = decoded.role;
                    _obj._id = decoded._id;
                    adminSchema.findOne({ _id: _obj._id }, function(error, response) {
                        if (error) handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
                        if (response && response.role == "SUPERADMIN") {
                            req.superAdmin = _obj;
                            next();
                        } else
                            return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.NOT_SUPER_ADMIN);
                    })
                } else {
                    return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.NOT_SUPER_ADMIN);
                }
            }
        })
    },
    recaptcha: function(req, res, next) {
        if (!config.recaptcha.isEnabled) return next();
        recaptcha.verify(req, function(error, data) {
            if (!error)
                next();
            else
                return handler.sendErr(handler.CAPTCHA_FAILED, res);
        });
    },
};

module.exports = obj;