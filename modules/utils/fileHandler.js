var multer = require('multer');
var multerS3 = require('multer-s3')
var handler = require('./handler');
var HttpStatus = require('http-status-codes');
var path = require('path');
var fs = require('fs-extra');
var aws = require('aws-sdk');
var config = require('../../config.js');
var voucher_codes = require('voucher-code-generator');
var kycFunc = require('../model_functions/kyc'); // kyc model function
aws.config.update({
    secretAccessKey: config.awssdk.secretAccessKey,
    accessKeyId: config.awssdk.accessKeyId,
    region: config.awssdk.region
});
const MAX_FILE_SIZE = 20 * 1024 * 1024;

var s3 = new aws.S3();

var MAGIC_NUMBERS = {
    jpg: 'ffd8ffe0',
    jpg1: 'ffd8ffe1',
    png: '89504e47',
    gif: '47494638'
};

var upload = multer({
    limits: { fileSize: MAX_FILE_SIZE },
    storage: multerS3({
        s3: s3,
        bucket: 'exchange-kyc',
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        metadata: function(req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function(req, file, cb) {
            console.log('timestamp - ' + Date.now().toString());
            cb(null, voucher_codes.generate({
                length: 6
            }) + Date.now().toString() + voucher_codes.generate({
                length: 6
            }))
        }
    }),
    fileFilter: function(req, file, cb) {
        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        if (mimetype && extname) {
            return cb(null, true);
        }
        cb("Error: File upload only supports the following filetypes - " + filetypes);
    }
})

function checkMagicNumbers(magic) {
    if (magic == MAGIC_NUMBERS.jpg || magic == MAGIC_NUMBERS.jpg1 || magic == MAGIC_NUMBERS.png || magic == MAGIC_NUMBERS.gif) return true
}

var obj = {
    uploadKyc: function(req, res, next) {
        console.log(req.query);
        upload.single('kyc')(req, res, function(err) {
            if (err) {
                handler.sendErr(HttpStatus.BAD_REQUEST, res, err);
            } else {
                if (req.file) {
                    next();
                } else return handler.sendErr(HttpStatus.BAD_REQUEST, res, handler.FILE_NOT_ADDED);
            }
        })
    },
    fileExistanceCheck: function(req, res, next) {
        var docTypes = ["identity", "image", "passport", "bank"];
        var user = req.user;
        if (docTypes.indexOf(req.query.docType) < 0) { // imagine someone typed wrong query param name.
            console.log('not in array');
            return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.INVALID_PARAMS);
        } else {
            kycFunc.findOne({ "userId": user.user_id }, { "kyc": 1 }, "", function(err, data) {
                if (err)
                    return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
                else {
                    if (data.kyc[req.query.docType].number) { // checking if docs already present.
                        console.log('already in db');
                        return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.INVALID_ACTION);
                    } else {
                        console.log('success');
                        next();
                    }
                }
            });
        }
    }
}

module.exports = obj;