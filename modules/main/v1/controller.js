/**
 * Created by karankohli13 on 05/11/17.
 */

var bluebird = require('bluebird'),
    adminFunc = require('../../model_functions/admin'), // admin model function
    kycFunc = require('../../model_functions/kyc'), // kyc model function
    keytool = require('../../utils/keyTool'),
    handler = require('../../utils/handler'),
    date = require('../../utils/datesHelper'),
    smsSender = require('../../utils/smsSender'),
    mailer = require('../../utils/mailer'),
    config = require('../../../config'),
    HttpStatus = require('http-status-codes'),
    jsonwebtoken = require('jsonwebtoken'),
    asyncLoop = require('node-async-loop'),
    bcrypt = require('bcrypt-nodejs'),
    _ = require('lodash'),
    util = require('util'),
    path = require('path'),
    request = require('request');
var redis = require('redis');
var redisClient = redis.createClient({ host: 'localhost', port: config.redis });


function Controller() {}

Controller.prototype.login = function(req, res) {
    var body = req.body;
    var obj = {
        email: body.username,
        password: body.password,
    };
    if (!obj.email || !obj.password) return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.PARAMS_MISSING);

    adminFunc.find({}, "", "", function(error, response) {
        if (error) return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, error.message);
        var len = response.length;
        if (len < 1) { // First user (Very first login to the system will be the super user)
            obj["role"] = "SUPERADMIN";
            adminFunc.create(obj, function(err, resp) {
                if (err) return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
                var loginToken = keytool.createToken(resp);
                return handler.sendRes({ token: loginToken }, res, 'Super Admin Created..');
            });
        } else {
            adminFunc.findOne({ "email": obj.email }, "_id password role", "", function(err, data) {
                if (err) return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
                if (!data) {
                    return handler.sendErr(HttpStatus.NOT_FOUND, res, handler.ADMIN_NOT_FOUND);
                }
                if (data && data.comparePassword(obj.password)) {
                    var loginToken = keytool.createToken(data);
                    handler.sendRes({ token: loginToken }, res, 'Login successful');
                } else {
                    return handler.sendErr(HttpStatus.UNAUTHORIZED, res, handler.WRONG_PSWD);
                }
            });
        }
    })
}

Controller.prototype.getMyrole = function(req, res) {
    var admin = req.admin;
    adminFunc.findOne({ "_id": admin._id }, "role", "", function(err, data) {
        if (err) return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        if (data.length == 0)
            return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, handler.NO_RECORDS);
        handler.sendRes(data.role, res, `Admin Role fetched successfully.`);
    });
}

Controller.prototype.forgot = function(req, res) {
    var body = req.body;
    adminFunc.findOne({ "email": body.username }, { "_id": 1,}, "", function(err, data) {
        if (err)
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        else {
            if (!data || data === null)
                return handler.sendErr(HttpStatus.NOT_FOUND, res, handler.ADMIN_NOT_FOUND);
            else {
                handler.sendRes('', res, "Email sent successfully");
                var forgotToken = keytool.createToken(data);
                console.log(forgotToken);
                /*var opts = {
                    email: data.email.value,
                    type: 'user-forgot',
                    subject: 'GladAge Password reset.',
                    data: {
                        name: data.fullName,
                        company: config.emailSender[0].mailData.company,
                        mailer: 0
                    },
                    link: 'https://tokensale.gladage.com/#/forgetPassword/' + forgotToken
                }
                mailer.send(opts);*/
            }
        }

    });
}

Controller.prototype.forgetPassword = function(req, res) {
    var body = req.body;
    var admin = req.admin;
    if (body.newPassword != body.confPassword)
        handler.sendErr(HttpStatus.FORBIDDEN, res, handler.WRONG_CNF_PSWD);
    else {
        var hash = bcrypt.hashSync(body.newPassword);
        adminFunc.findOneAndUpdate({ "_id": admin._id }, {
                "password": hash,
            },
            function(err, data) {
                if (err) {
                    handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
                } else {
                    handler.sendRes('', res, handler.PSWD_SET);
                    /*var opts = {
                        email: user.email.value,
                        type: 'user-resetPswd',
                        subject: 'GladAge Password Reset Confirmation.',
                        data: {
                            company: config.emailSender[0].mailData.company,
                            support: config.emailSender[0].mailData.support,
                            mailer: 0
                        },
                        link: 'https://tokensale.gladage.com/#/forgetPassword/' + forgotToken
                    }
                    mailer.send(opts);*/
                }
            });
    }
};




// SUPERADMIN ROUTES

Controller.prototype.createSupervisor = function(req, res) {
    var body = req.body;
    var admin = req.superAdmin;
    var obj = {
        password: body.password,
        email: body.email,
        createdBy: admin._id
    }
    if (!obj.password || !obj.email) return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.PARAMS_MISSING);
    adminFunc.create(obj, function(err, data) {
        if (err) {
            if (err.code == 11000)
                return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.ALREADY_EXISTS);
            else
                return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        }
        if (data) {
            var token = keytool.createToken(data);
            handler.sendRes({ token: token }, res, "Admin created");
        }
    });
}

///get all tge skipped kyc's
Controller.prototype.getSkippedKyc = function(req, res) {
    kycFunc.find({ "kycStatus": 'skipped' }, "", "", function(err, data) {
        if (err) return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        if (data.length == 0)
            return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, handler.NO_RECORDS);
        handler.sendRes(data, res, `Kyc's are fetched successfully.`);
    });
}

//Set the status on the skipped kyc's only valid for the superadmins
Controller.prototype.changeStatusSkipped = function(req, res) {
    var admin = req.admin;
    kycFunc.findOneAndUpdate({ "_id": body._id }, {
        "kycStatus": body.action,
        "kycVerifiedBy": admin._id,
        "kycComment": body.comment,
        "verifiedOn": date.unixTimestamp()
    }, function(err, data) {
        if (err)
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        else {
            handler.sendRes({ "success": true }, res, "Kyc updated...");
        }
    });
}



// ADMIN ROUTES
Controller.prototype.getAllUsers = function(req, res) {
    kycFunc.find({}, "createdAt kycStatus kycVerifiedBy userId", "", function(err, data) {
        if (err) return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        if (data.length == 0)
            return handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, handler.NO_RECORDS);
        handler.sendRes(data, res, `Fetched successfully.`);
    });
}


Controller.prototype.getPrefsAdmin = function(req, res) {
    var query = req.query;
    kycFunc.findOne({ "userId": query.id }, "kycStatus fullName email.value kyc kycComment", "", function(err, data) {
        if (err) {
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        } else {
            if (!data || data == null)
                handler.sendErr(HttpStatus.NOT_FOUND, res, handler.USER_NOT_FOUND);
            else
                handler.sendRes(data, res, "User prefs fetched successfully");
        }
    });
};

Controller.prototype.setPrefsAdmin = function(req, res) {
    var body = req.body;
    kycFunc.findOneAndUpdate({ "userId": body.userId }, body, function(err, kyc) {
        if (err) {
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        } else {
            handler.sendRes('', res, "User info updated successfully");
            if (kyc.kycStatus == 'processing') {
                redisClient.rpush('exchange_kyc_list', JSON.stringify({ "kyc_id": kyc._id, "status": kyc.kycStatus }));
                /*var opts1 = {
                    email: user.email.value,
                    type: 'kyc-update',
                    subject: 'xxxxxxxxx',
                    data: {
                        text: `We've recieved your Documents and kyc details for verfication. Once done you'll be notified for the same.`,
                        mailer: 0
                    }
                }*/
                // mailer.send(opts1);
            }
        }
    });
};

Controller.prototype.saveKycDetailsAdmin = function(req, res) {
    var doctype = req.query.docType;
    var query = req.query;
    var admin = req.user;
    var queryStringPicture = `kyc.${doctype}.picture`;
    var queryStringNumber = `kyc.${doctype}.number`;

    kycFunc.findOneAndUpdate({ "userId": query.userid }, {
        [queryStringPicture]: req.file.location,
        [queryStringNumber]: query.number
    }, function(err, data) {
        if (err) {
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        } else {
            handler.sendRes('', res, handler.DOC_UPLOADED);
        }
    });
};



Controller.prototype.getUserKyc = function(req, res) {
    var admin = req.admin;
    redisClient.get(admin._id, function(err, user_kyc) { // Checking if admin is already present in REDIS LIST
        if (!user_kyc) {
            console.log("Admin not in admin_list");
            redisClient.lpop('exchange_kyc_list', function(err, kyc) {
                if (!kyc) {
                    console.log("no data in kyc_list");
                    handler.sendRes([], res, "Fetched users Kyc successfully")
                } else {
                    redisClient.set(admin._id, JSON.parse(kyc).kyc_id); // Mapping user_kyc with Admin in redis ...
                    console.log('POPPED KYC_LIST - KYC_id - ' + JSON.parse(kyc).kyc_id);
                    serveKyc(JSON.parse(kyc).kyc_id);
                }
            });
        } else {
            console.log(user_kyc); // Serving usr_kyc associated with the admin present in the REDIS LIST
            serveKyc(user_kyc);
        }
    });

    function serveKyc(user_kyc) { // serving kyc function ..
        kycFunc.find({ "_id": user_kyc, "kycStatus": "processing" }, "", "", function(err, data) {
            if (err)
                handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
            else {
                handler.sendRes(data, res, "Fetched Kyc successfully")
            }
        });
    }
};

Controller.prototype.updateUserKyc = function(req, res) {
    var body = req.body;
    var admin = req.admin;
    var data = {
        "kycStatus": body.action,
        "kycVerifiedBy": admin._id,
        "kycComment": body.comment,
        "verifiedOn": date.unixTimestamp()
    };
    kycFunc.findOneAndUpdate({ "_id": body.id }, data, function(err, resp) {
        if (err)
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        else {
            redisClient.DEL(admin._id);
            handler.sendRes({ "success": true }, res, "Kyc status updated...");
        }
    });
};


// USER. (Uploading KYC and DOCS)

Controller.prototype.saveKycDetails = function(req, res) {
    var doctype = req.query.docType;
    var query = req.query;
    var user = req.user;
    var queryStringPicture = `kyc.${doctype}.picture`;
    var queryStringNumber = `kyc.${doctype}.number`;

    kycFunc.findOneAndUpdate({ "userId": user.user_id }, {
        [queryStringPicture]: req.file.location,
        [queryStringNumber]: query.number
    }, function(err, data) {
        if (err) {
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        } else {
            handler.sendRes('', res, handler.DOC_UPLOADED);
        }
    });
};

Controller.prototype.saveBankDetails = function(req, res) {
    var body = req.body;
    var user = req.user;
    kycFunc.findOne({ "userId": user.user_id }, "kyc.bank _id", "", function(err, data) {
        if (err)
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        if (data) {
            if (!data.bankDetail.accNumber) {
                kycFunc.findOneAndUpdate({ "userId": user.user_id }, {
                        "bankDetail.accNumber": body.accNumber,
                        "bankDetail.ifsc": body.ifsc,
                        "bankDetail.bankName": body.bankName
                    },
                    function(e, r) {
                        if (!e)
                            handler.sendRes('', res, "Your Bank Details uploaded successfully..");
                    });
            } else return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.INVALID_ACTION);
        }
    })
};
Controller.prototype.saveAddressDetails = function(req, res) {
    var body = req.body;
    var user = req.user;
    kycFunc.findOne({ "userId": user.user_id }, "address _id", "", function(err, data) {
        if (err)
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        if (data) {
            if (!data.address) {
                kycFunc.findOneAndUpdate({ "userId": user.user_id }, {
                        "address.flat": body.flat,
                        "address.street": body.street,
                        "address.state": body.state,
                        "address.pincode": body.pincode,
                        "address.country": body.country,
                    },
                    function(e, r) {
                        if (!e)
                            handler.sendRes('', res, "Your Address details uploaded successfully..");
                    });
            } else return handler.sendErr(HttpStatus.FORBIDDEN, res, handler.INVALID_ACTION);
        } else {
            kycFunc.create({
                "userId": user.user_id,
                "address.flat": body.flat,
                "address.street": body.street,
                "address.state": body.state,
                "address.pincode": body.pincode,
                "address.country": body.country,
            }, function(e, data) {
                if (!e)
                    handler.sendRes('', res, "Your Address details uploaded successfully..");
            })
        }
    })
};

Controller.prototype.getPrefsUser = function(req, res) {
    var user = req.user;
    kycFunc.findOne({ "userId": user.user_id }, "kycStatus kyc kycComment", "", function(err, data) {
        if (err) {
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        } else {
            if (!data || data == null)
                handler.sendErr(HttpStatus.NOT_FOUND, res, handler.USER_NOT_FOUND);
            else
                handler.sendRes(data, res, "User prefs fetched successfully");
        }
    });
};

Controller.prototype.setPrefsUser = function(req, res) {
    var body = req.body;
    var user = req.user;
    kycFunc.findOneAndUpdate({ "userId": user.user_id }, body, function(err, user) {
        if (err) {
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        } else {
            handler.sendRes('', res, "User info updated successfully");
            if (user.kycStatus == 'processing') {
                redisClient.rpush('exchange_kyc_list', JSON.stringify({ "kyc_id": user._id, "status": user.kycStatus }));
                /*var opts1 = {
                    email: user.email.value,
                    type: 'kyc-update',
                    subject: 'xxxxxxxxx',
                    data: {
                        text: `We've recieved your Documents and kyc details for verfication. Once done you'll be notified for the same.`,
                        mailer: 0
                    }
                }*/
                // mailer.send(opts1);
            }
        }
    });
};

///the function to get the kyc details of the user
Controller.prototype.getKycDetails = function(req, res) {
    var user = req.user;
    kycFunc.findOne({ "userId": user.user_id }, "kyc address bankDetail kycStatus", "", function(err, data) {
        if (err) {
            handler.sendErr(HttpStatus.INTERNAL_SERVER_ERROR, res, err.message);
        } else
            handler.sendRes(data, res, "Your KYC Details fetched successfully..");
    });
};

module.exports = new Controller();