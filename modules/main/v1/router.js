/**
 * Created by karankohli13 on 05/11/17.
 */

let express = require('express'),
    config = require('../../../config'),
    router = express.Router(),
    controller = require('./controller'),
    auth = require('../../utils/keyTool'),
    validator = require('../../utils/validator'),
    upload = require('../../utils/fileHandler');



/* --------------   DESKTOP ROUTES START ----------------  */
router

    //Login
    .post('/admin/login', auth.recaptcha, validator.checkLogin, controller.login)
    .get('/admin/getMyrole', auth.verifyAllAdmin, controller.getMyrole)
    .post('/admin/forgot', controller.forgot)
    .put('/admin/forgetPassword/:token?', auth.verifyAllAdmin, validator.checkPassForget, controller.forgetPassword)

    // Super Admin
    .post('/superadmin/createSupervisor', auth.verifySuperAdmin,validator.checkSignup, controller.createSupervisor)
    .post('/superadmin/changeStatusSkipped', auth.verifySuperAdmin, controller.changeStatusSkipped)
    .post('/superadmin/getSkippedKyc', auth.verifySuperAdmin, controller.getSkippedKyc)
    // Admin
    .get('/admin/getUserKyc', auth.verifyAdmin, controller.getUserKyc)
    .post('/admin/updateUserKyc', auth.verifyAdmin, controller.updateUserKyc)

    .get('/admin/getPrefs', auth.verifyAllAdmin, controller.getPrefsAdmin)
    .put('/admin/setPrefs', auth.verifyAllAdmin, controller.setPrefsAdmin)

    .get('/admin/getAllUsers', auth.verifyAllAdmin, controller.getAllUsers)
    .post('/admin/saveKycDetailsAdmin', auth.verifyAllAdmin, upload.fileExistanceCheck, upload.uploadKyc, controller.saveKycDetailsAdmin)

    // User file Upload
    .get('/user/getPrefs', auth.verifyUser, controller.getPrefsUser)
    .put('/user/setPrefs', auth.verifyUser, controller.setPrefsUser)
    .post('/user/saveKycDetails', auth.verifyUser, upload.fileExistanceCheck, upload.uploadKyc, controller.saveKycDetails)
    .post('/user/saveBankDetails', auth.verifyUser, controller.saveBankDetails)
    .post('/user/saveAddressDetails', auth.verifyUser, controller.saveAddressDetails)
    .get('/user/getKycDetails', auth.verifyUser, controller.getKycDetails);


/* --------------   DESKTOP ROUTES END ----------------  */

module.exports = {
    desktop: router
};