/**
 * Created by karankohli13 on 5/11/17.
 */

var config = require('./config');
var validator = require('./modules/utils/validator');
var bodyparser = require('body-parser');
var mongoose = require('mongoose');
var express = require('express');
var path = require('path');
var app = express();
var http = require('http');
var util = require('util');
var server = http.createServer(app);
mongoose.Promise = require('bluebird');
var mongoSanitize = require('mongo-express-sanitize');
var helmet = require('helmet');
const expressValidator = require('express-validator');
const requestIp = require('request-ip');

app.use(helmet());
app.use(bodyparser.json({ limit: '50mb' }));
app.use(bodyparser.urlencoded({ extended: true, limit: '50mb' }));
app.use(requestIp.mw());
app.use(expressValidator(validator.customSanitizers));
app.use(mongoSanitize.default());

var redis = require('redis');
var redisClient = redis.createClient({ host: 'localhost', port: config.redis });

redisClient.on('ready', function() {
    console.log("Redis  running at - " + config.redis);
});

redisClient.on('error', function() {
    console.log("Error in Redis");
});

var connection = mongoose.connection;
connection.on('error', function(error) {
    //console.error.bind(console, 'error connecting with mongodb database:')
    console.log('Mongodb Connection Error');
    console.log(error);
});

connection.once('open', function() {
    console.log('Database connected event log');
});

connection.on('disconnected', function() {
    //Reconnect on timeout
    mongoose.connect(config.db, {
        server: {
            auto_reconnect: true,
            socketOptions: {
                keepAlive: 500,
                connectTimeoutMS: 90000,
                socketTimeoutMS: 90000
            }
        }
    });
}, function(err) {
    if (err)
        console.log(err);
    else {
        console.log('Database connected after disconnect');
    }
    connection = mongoose.connection;
});

mongoose.connect(config.db, {
    server: {
        auto_reconnect: true,
        socketOptions: {
            keepAlive: 500,
            connectTimeoutMS: 90000,
            socketTimeoutMS: 90000
        },
        connectWithNoPrimary: true
    }
}, function(err) {
    if (err) {
        console.log('Mongodb connection error 1st');
        console.log(err);
    } else {
        console.log('Database connected');
    }

});


server.listen(config.port, function(err) {
    if (err)
        console.log(err);
    else
        console.log('Server running at - ' + config.port);
});


var cors = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "accept, content-type, x-access-token, x-requested-with");
    next();
};

app.use(cors);

//BootStrap our router
app.use(require('./modules/mainRouter'));



// logoz.io configurations

/*var logger = require('logzio-nodejs').createLogger({
    token: 'VRoglUmyxMtJbaNkUwHCtWTuceqnKisZ',
    host: 'listener.logz.io',
    type: 'YourLogType' // OPTIONAL (If none is set, it will be 'nodejs')
});*/