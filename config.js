var path = require('path'),
    nodemailer = require('nodemailer'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';


var config = {
    development: {
        root: rootPath,
        app: {
            name: 'exchange-kyc',
            desktopApi: 'v1',
            mobileApi: 'v1'
        },
        port: process.env.PORT || 3030,
        db: 'mongodb://localhost/exchange-kyc',
        redis: '6379',
        awssdk: {
            secretAccessKey: 'E0D4pja3csAXE7pf2Ic/tBoUmHQGtFSOPMNaxtQX',
            accessKeyId: 'AKIAITXWYYIRW3B7AZ7A',
            region: 'us-east-1'
        },
        userauth :'http://192.168.1.136:3005/v1/verify/user/exist',
        smsSender: {
            twilio: {
                accountSid: process.env.twilioAccountSid || '',
                authToken: process.env.twilioAuthToken || '',
                number: ''
            },
            msg91: {
                authkey: process.env.msg91Authkey || ''
            }
        },
        emailSender: {
            mailData: {
                company: 'XXXX',
                label: 'XXXX',
                subject: 'XXXX',
                logo: 'XXXX',
                support: 'XXXX'
            },
            sender: 'XXXX',
            transporter: nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'XXXX',
                    pass: 'XXXX'
                }
            })
        },
        recaptcha: {
            isEnabled: false,
            site_key: '',
            secret_key: ''
        }
    },
    production: {
        root: rootPath,
        app: {
            name: 'exchange-kyc',
            desktopApi: 'v1',
            mobileApi: 'v1'
        },
        port: process.env.PORT || 3030,
        db: 'mongodb://localhost/exchange-kyc',
        redis: '6379', // compose redis
        awssdk: {
            secretAccessKey: 'E0D4pja3csAXE7pf2Ic/tBoUmHQGtFSOPMNaxtQX',
            accessKeyId: 'AKIAITXWYYIRW3B7AZ7A',
            region: 'us-east-1'
        },
        smsSender: {
            twilio: {
                accountSid: process.env.twilioAccountSid || '',
                authToken: process.env.twilioAuthToken || '',
                number: ''
            },
            msg91: {
                authkey: process.env.msg91Authkey || ''
            }
        },
        emailSender: {
            mailData: {
                company: 'XXXX',
                label: 'XXXX',
                subject: 'XXXX',
                logo: 'XXXX',
                support: 'XXXX'
            },
            sender: 'XXXX',
            transporter: nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'XXXX',
                    pass: 'XXXX'
                }
            })
        },
        recaptcha: {
            isEnabled: false,
            site_key: '',
            secret_key: ''
        }
    }

};


module.exports = config[env];